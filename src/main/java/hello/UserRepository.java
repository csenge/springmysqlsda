package hello;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<User, Integer> {

    @Query(value = "SELECT * FROM user WHERE name=:desiredName",
            nativeQuery = true)
    List<User> getUserByNAme(String desiredName);


     default User updateUser(Integer id, User user) {
        Optional<User> userToUpdate = findById(id);

        if (userToUpdate.isPresent()) {
            User userWithIdFound = userToUpdate.get();
            userWithIdFound.setName(user.getName());
            userWithIdFound.setEmail(user.getEmail());
            return save(userWithIdFound);
        }

        return save(user);
    }
}
