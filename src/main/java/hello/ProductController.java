package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Controller
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @GetMapping("/getAll")
    public @ResponseBody Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @PostMapping("/addProduct")
    public @ResponseBody Product addProduct(@RequestBody Product product) {
        return productRepository.save(product);
    }

    @GetMapping("/getById/{id}")
    public Product findProductById(@PathVariable Integer id) {
        Optional<Product> productOptional = productRepository.findById(id);

        if (productOptional.isPresent()) {
            return productOptional.get();
        } else {
            return null;
        }
    }
}
